# Terminus Secrets Plugin

- For Terminus 1.x version, use the [1.x branch](https://github.com/pantheon-systems/terminus-secrets-plugin/tree/1.x).
- For Terminus 0.x version, use the [0.x branch](https://github.com/pantheon-systems/terminus-secrets-plugin/tree/0.x).

